import { UPDATE_DAY } from '../actions/daySelectedActions';

export default function daySelectedReducer(state = '', {type, payload}){
    switch(type){
      case UPDATE_DAY:
        return payload.daySelected;
      default:
        return state;  
    }
  }