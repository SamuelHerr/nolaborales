import { UPDATE_DAYS } from '../actions/daysActions';

export default function noWorkingDaysReducer(state = '', {type, payload}){
    switch(type){
      case UPDATE_DAYS:
        return payload.noWorkingDays;
      default:
        return state;  
    }
  }