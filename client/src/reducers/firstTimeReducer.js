import { UPDATE_FIRST_TIME } from '../actions/firstTimeActions';

export default function firstTimeReducer(state = '', {type, payload}){
    switch(type){
      case UPDATE_FIRST_TIME:
        return payload.firstTime;
      default:
        return state;  
    }
  }