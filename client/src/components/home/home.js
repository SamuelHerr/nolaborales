import React from 'react';
import { Navbar, Container, Row , Col} from 'react-bootstrap';
import DayList from './dayList';
import EmptySpace from './emptySpace';
import EditDayForm from './editDay';
import axios from 'axios';
import { APIHOST as domain } from '../../app.json';

import { connect } from 'react-redux';

import { updateFirstTime } from '../../actions/firstTimeActions';
import { updateNoWorkingDays } from '../../actions/daysActions';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editando: false
        }
        this._handleChildClick = this._handleChildClick.bind(this); 
    }

    componentWillMount(){
        this.loadDays();
    }

    _handleChildClick() {
        this.loadDays();
    }

    loadDays(){
        axios.get(`${domain}`).then(response => {
            if(response.data.length > 0){
                this.props.onUpdateNoWorkingDays(response.data);
                this.props.onUpdateFirstTime(false);
            }else{
                this.props.onUpdateFirstTime(true);
            }
        })
        .catch(error =>{
            console.log(error);
        })
    }

    render() { 
        return (  
           
            <Container id="home-container" fluid>
                <Navbar bg="primary" variant="dark">
                    <Navbar.Brand>
                        Listado de días no laborales - 
                    </Navbar.Brand>
                </Navbar>
                { this.props.firstTime ?
                    <EmptySpace handleClick={this._handleChildClick}></EmptySpace> 
                    :
                    <Row style={{"marginTop": "100px"}}>
                        <Col>
                            <DayList days={this.props.noWorkingDays}></DayList>
                        </Col>
                        <Col>
                            <EditDayForm selectedDay={this.props.dayToEdit} key={this.props.dayToEdit ? this.props.dayToEdit._id : 0 }></EditDayForm>
                        </Col>
                    </Row> 
                    
                }                                                         
            </Container>
        );
    }
}


const mapStateToProps = state => ({
    noWorkingDays: state.noWorkingDays,
    firstTime: state.firstTime,
    dayToEdit: state.dayToEdit
});

const mapActionsToProps = {
    onUpdateNoWorkingDays: updateNoWorkingDays,
    onUpdateFirstTime: updateFirstTime
}
  
export default connect(mapStateToProps,mapActionsToProps)(Home);