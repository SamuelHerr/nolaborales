import React from 'react';
import { Container, ListGroup } from 'react-bootstrap';
import DayItem from './day';


export default class DayList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            day: {
                titulo: 'This is a title'
            }
          }
    }
    render() { 
        const listItems = this.props.days.map( day => 
            <DayItem key={day._id} day={ day }></DayItem>
        );
        return (  
            <Container id="home-container">
                <ListGroup>
                    {listItems}
                </ListGroup>
            </Container>
        );
    }
}