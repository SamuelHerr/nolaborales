import React from 'react';
import { ListGroup,Row, Col, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { updateDay } from '../../actions/daySelectedActions';


class DayItem extends React.Component {

    onUpdateDay(){
        this.props.onUpdateDay(this.props.day);
        // this.props.handleClick();
    }

    render() { 
        return (  
            <ListGroup.Item> 
                <Row>
                    <Col><span>{this.props.day.dia}/{this.props.day.mes}  </span></Col>
                    <Col xs={8}>{this.props.day.motivo} </Col>
                    <Col>
                        <Button variant="info" onClick={() => this.onUpdateDay() }> Edit </Button>
                    </Col>
                </Row>
            </ListGroup.Item>
        );
    }
}

const mapStateToProps = state => ({
    dayToEdit: state.dayToEdit,
});

const mapActionsToProps = {
    onUpdateDay: updateDay,
}
  
export default connect(mapStateToProps,mapActionsToProps)(DayItem);