import React from 'react';
import { Jumbotron , Button } from 'react-bootstrap';
import axios from 'axios';
import { APIHOST as domain } from '../../app.json';

import { connect } from 'react-redux';

import { updateFirstTime } from '../../actions/firstTimeActions';

class EmptySpace extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }

    importDays(){
        //post to localhost:3001 para realizar el init 
        axios.post(`${domain}/init`)
             .then(response => {
                 alert(response.data.mensaje);    
                 this.props.onUpdateFirstTime(false);   
                 this.props.handleClick();       
             })
             .catch(function(error){
                console.log(error);
             });
    }

    render() { 
        return (  
            <Jumbotron>
                <h1>Hola, veo que es la primera vez que corres esta plataforma!</h1>
                <p>
                    Puedes realizar la importación de datos al dar click al siguiente botón:
                </p>
                <p>
                    <Button variant="primary" onClick = { () => this.importDays() } > 
                        Inicializar
                    </Button>
                </p>
            </Jumbotron>
        );
    }
}


const mapStateToProps = state => ({
    firstTime: state.firstTime,
});

const mapActionsToProps = {
    onUpdateFirstTime: updateFirstTime
}
  
export default connect(mapStateToProps,mapActionsToProps)(EmptySpace);