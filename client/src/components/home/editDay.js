import React from 'react';
import { Card,Container, Form, Button } from 'react-bootstrap';
import axios from 'axios';
import { APIHOST as domain } from '../../app.json';
import { connect } from 'react-redux';
import { updateDay } from '../../actions/daySelectedActions';

 class EditDayForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            motivo: '',
            dia: '',
            mes: '',
            tipo:'',
            info: '',
            maxDays: 30
         }

         this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        if(name === 'mes')
            this.setState({maxDays: this.getMaxDayByMonth() })
        
        
        this.setState({
            [name]: value
          });
      }

    componentDidMount(){
        this.getDay()
    }

    getDay(){
        axios.get(`${domain}/${this.props.dayToEdit._id}`).then(response => {
            if(response.data){
                this.setState({motivo: response.data.motivo,
                               dia: response.data.dia,
                               mes: response.data.mes,
                               tipo: response.data.tipo,
                               info: response.data.info
                            })
            }else{
                console.log('No hay día seleccionado') 
            }
        })
        .catch(error =>{
            console.log(error);
        })
    }

    onUpdateDay(){
        this.getDay();
    }

    onEditButton(){
        axios.put(`${domain}/${this.props.dayToEdit._id}`,
                {
                    motivo: this.state.motivo,
                    dia: this.state.dia,
                    mes: this.state.mes,
                    tipo: this.state.tipo,
                    info: this.state.info

                }).then(response => {
                    console.log(response)
                })
                .catch(error =>{
                    console.log(error);
                })
    }

    onCancelButton(){
        this.setState({motivo:'',
            dia: '',
            mes: '',
            tipo: '',
            info: ''
         })
    }

    getMaxDayByMonth(){
            var month = this.state.mes - 1; 
            var year = 2020; 
            return new Date(year, month, 0).getDate()  
    }

    render() { 
        return (  
            <Container fluid>
                
                    {  this.state.motivo !== '' ? 
                        <Card>
                            <Card.Body>
                                <Form>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Motivo</Form.Label>
                                        <Form.Control name="motivo" value={this.state.motivo} onChange={this.handleChange}/> 
                                    </Form.Group>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Mes</Form.Label>
                                        <Form.Control name="mes" value={this.state.mes} onChange={this.handleChange} type="number" max="12"/> 
                                    </Form.Group>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Día</Form.Label>
                                        <Form.Control name="dia" value={this.state.dia} onChange={this.handleChange} type="number" max={this.state.maxDays} /> 
                                    </Form.Group>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Tipo</Form.Label>
                                        <Form.Control name="tipo" value={this.state.tipo} onChange={this.handleChange}/> 
                                    </Form.Group>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Info</Form.Label>
                                        <Form.Control name="info" value={this.state.info} onChange={this.handleChange}/> 
                                    </Form.Group>  
                                    <Button variant="danger" onClick={() => this.onCancelButton()}>
                                        Cancelar
                                    </Button>               
                                    <Button variant="primary" type="submit" onClick={() => this.onEditButton()}>
                                        Editar
                                    </Button>

                                </Form>
                            </Card.Body>  
                        </Card> 
                        :
                        <span></span>

                    }
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    dayToEdit: state.dayToEdit,
});

const mapActionsToProps = {
    onUpdateDay: updateDay,
}
  
export default connect(mapStateToProps,mapActionsToProps)(EditDayForm);