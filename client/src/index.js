import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { combineReducers, createStore } from 'redux';

import { Provider } from 'react-redux';

import noWorkingDaysReducer from './reducers/daysReducer';
import daySelectedReducer from './reducers/daySelectedReducer';
import firstTimeReducer from './reducers/firstTimeReducer';


const allReducers = combineReducers({
  noWorkingDays: noWorkingDaysReducer,
  firstTime: firstTimeReducer,
  dayToEdit: daySelectedReducer
});

const store = createStore(
  allReducers,
  {
    noWorkingDays: [],
    firstTime: true,
    dayToEdit: {
      _id: 0
    }
  },
  window.devToolsExtension && window.devToolsExtension()
);

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
