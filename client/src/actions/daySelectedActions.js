export const UPDATE_DAY = 'day:updateDay';

export function updateDay(newDay){
    return{
        type: UPDATE_DAY,
        payload:{
            daySelected: newDay
        }
    }
}