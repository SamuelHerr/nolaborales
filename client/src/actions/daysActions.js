export const UPDATE_DAYS = 'noWorkingDays:updatenoWorkingDays';

export function updateNoWorkingDays(newNoWorkingDays){
    return{
        type: UPDATE_DAYS,
        payload:{
            noWorkingDays: newNoWorkingDays
        }
    }
}