export const UPDATE_FIRST_TIME = 'firstTime:updateFirstTime';

export function updateFirstTime(newFirstTime){
    return{
        type: UPDATE_FIRST_TIME,
        payload:{
            firstTime: newFirstTime
        }
    }
}