const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NoLaboralesSchema = new Schema({
    mes: {type: Number},
    dia: {type: Number},
    motivo: {type: String},
    tipo: {type: String},
    info: {type: String},
    opcional: {type: String},
    religion: {type: String}
});

module.exports = mongoose.model("noLaborales",NoLaboralesSchema);