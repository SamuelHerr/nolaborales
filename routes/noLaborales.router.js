const express = require('express');
const router = express.Router();
const noLaboralesController = require('../controllers/noLaborales.controller');
const noLaboralesModel = require('../models/noLaborales.model');

router.post("/",noLaboralesController.create);
router.get("/",noLaboralesController.find);
router.get("/:id",noLaboralesController.findOne);
router.put("/:id",noLaboralesController.update);
router.delete("/:id",noLaboralesController.removeOne);

router.post("/init",noLaboralesController.initDB);

module.exports = router;