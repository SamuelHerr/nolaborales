# Días no laborales Argentina.

## Instalación

Existen dos proyectos dentro del mismo repositorio
- root:  Backend - Proyecto en nodejs + expressjs + mongoose
    - cd nolaborales; npm install;
- client/ Frontend - Proyecto en ReactJs
    - cd nolaborales; cd client; npm install;

## Ejecución

Es necesario realizar la ejecución de los proyectos de manera independiente:
- Backend - npm start
    - Generará un entorno para la API dentro del puerto 3001
- Frontend - npm start
    - Acceso a la plataforma por medio del puerto 3000


### librerias utilizadas
* NodeJs
* Nodemon
* ExpressJs
* CORS
* node-fetch
* Mongoose
* ReactJs
* Redux
* Axios
* Bootstrap
* React-Bootstrap



