const mongoose = require("mongoose");

const host = "localhost";
const port = "27017"; 
const db = "nolaborales";

exports.mongoConect = () => {
    const mongoStringConection = `mongodb://${host}:${port}/${db}`;
    mongoose.connect(mongoStringConection);
    mongoose.Promise = global.Promise;
    const dbConection = mongoose.connection;
    dbConection.on("error", console.log.bind(console, "There is an error in the conection with mongoDB"))
}