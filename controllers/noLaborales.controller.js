const NoLaborales = require('../models/noLaborales.model');
const noLaboralesModel = require('../models/noLaborales.model');

const fetch = require('node-fetch');


let response = {
    mensaje: "",
    completado: false
}

exports.initDB = function(req,res){
    fetch('http://nolaborables.com.ar/api/v1/2016?excluir=opcional')
        .then(res => res.json())
        .then(json => {
            NoLaborales.insertMany(json).catch(()=>{
                return
            });
            
        });
        response.completado = true;
        response.mensaje = "Día no laboral importados correctamente."
        res.json(response);
}

exports.create = function(req,res){
    let newDay = new NoLaborales({
        mes: req.body.mes,
        dia: req.body.dia,
        motivo: req.body.motivo,
        tipo: req.body.tipo,
        info: req.body.info,
        opcional: req.body.opcional,
        religion: req.body.religion
    });

    newDay.save(function(err){
        if(err){
            response.completado = false;
            response.mensaje = "Error: No se pudo guardar el día."
            console.error(err);
            res.json(response);
            return
        }
        response.completado = true;
        response.mensaje = "Día no laboral guardado correctamente."
        res.json(response);
    });    
}

exports.find = function(req,res){
    NoLaborales.find({}).sort('mes').exec(function(err, noLaborales){
        res.json(noLaborales);
    });
} 

exports.findOne = function(req,res){
    NoLaborales.findById({_id: req.params.id},function(err, noLaboral){
        res.json(noLaboral);
    });
} 

exports.update = function(req,res){
    let dayToUpdate = {
        mes: req.body.mes,
        dia: req.body.dia,
        motivo: req.body.motivo,
        tipo: req.body.tipo,
        info: req.body.info,
        opcional: req.body.opcional,
        religion: req.body.religion
    };

    NoLaborales.findByIdAndUpdate(req.params.id,{$set: dayToUpdate},function(err){
        if(err){
            response.completado = false;
            response.mensaje = "Error: No se pudo actualizar el día."
            console.error(err);
            res.json(response);
            return
        }
        response.completado = true;
        response.mensaje = "Día no laboral actualizado correctamente."
        res.json(response);
    });
}

exports.removeOne = function(req,res){
    NoLaborales.findByIdAndRemove({_id: req.params.id },function(err){
        if(err){
            response.completado = false;
            response.mensaje = "Error: No se pudo eliminar el día."
            console.error(err);
            res.json(response);
            return
        }
        response.completado = true;
        response.mensaje = "Día no laboral eliminado correctamente."
        res.json(response);
    })
}